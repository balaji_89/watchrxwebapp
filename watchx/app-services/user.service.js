﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};

        service.GetAll = authenticateUser;

        return service;

        function authenticateUser() {
            return $http.get('https://8-dot-watchrx-1007.appspot.com/api/users').then(handleSuccess, handleError('Error getting all users'));
        }


        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
